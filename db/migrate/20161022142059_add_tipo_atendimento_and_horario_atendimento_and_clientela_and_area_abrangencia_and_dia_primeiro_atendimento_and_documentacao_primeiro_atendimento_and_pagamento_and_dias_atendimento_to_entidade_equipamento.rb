class AddTipoAtendimentoAndHorarioAtendimentoAndClientelaAndAreaAbrangenciaAndDiaPrimeiroAtendimentoAndDocumentacaoPrimeiroAtendimentoAndPagamentoAndDiasAtendimentoToEntidadeEquipamento < ActiveRecord::Migration
  def change
    add_column :entidade_equipamentos, :tipo_atendimento, :string
    add_column :entidade_equipamentos, :horario_atendimento, :string
    add_column :entidade_equipamentos, :clientela, :string
    add_column :entidade_equipamentos, :area_abrangencia, :string
    add_column :entidade_equipamentos, :dia_primeiro_atendimento, :string
    add_column :entidade_equipamentos, :documentacao_primeiro_atendimento, :string
    add_column :entidade_equipamentos, :pagamento, :string
    add_column :entidade_equipamentos, :dias_atendimento, :string
  end
end
